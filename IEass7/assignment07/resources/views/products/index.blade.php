@extends('products.layout')

@section('content')



<h1>Products</h1>

<a class="btn btn-success btn-sm" href="{{ route('products.create') }}">Create new</a>
<br><br>

@if ($success = Session::get('success'))
<div class="alert alert-warning">
{{ $success }}
</div>
@endif

<table class="table">

@foreach ($products as $product)
<tr>
<td>
{{ $product->name }}
</td><td>
{{ $product->price }}€ 
</td><td>
<a class="btn btn-primary" href="{{ route('products.show', $product->id) }}">View</a>
</td><td>
<a class="btn btn-secondary" href="{{ route('products.edit', $product->id) }}">Edit</a>
</td><td>
<form action="{{ route('products.destroy', $product->id) }}" method="POST">
@csrf
@method('DELETE')
<input class="btn btn-danger" type="submit" value="Delete">
</form>
</td>
</tr>

@endforeach
</table>
@endsection
