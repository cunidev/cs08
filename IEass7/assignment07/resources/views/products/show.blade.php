@extends('products.layout')

@section('content')
<h1>{{ $product->name }}</h1>
<strong>Price: </strong> {{ $product->price }}€

<form action="{{ route('products.destroy', $product->id) }}" method="POST">
@csrf
@method('DELETE')
    <input type="submit" class="btn btn-danger" value="Delete">
    <a class="btn btn-secondary" href="{{ route('products.edit', $product->id) }}">Edit</a>
</form>


@endsection
