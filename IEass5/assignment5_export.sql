-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: May 11, 2020 at 12:06 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `assignment05`
--

-- --------------------------------------------------------

--
-- Table structure for table `COURSES`
--

CREATE TABLE `COURSES` (
  `Id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `duration` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `COURSES`
--

INSERT INTO `COURSES` (`Id`, `name`, `duration`) VALUES
(1, 'Math', 10),
(2, 'Science', 11),
(3, 'English', 14),
(4, 'Italian', 13);

-- --------------------------------------------------------

--
-- Table structure for table `GRADES`
--

CREATE TABLE `GRADES` (
  `student_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `grade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `GRADES`
--

INSERT INTO `GRADES` (`student_id`, `course_id`, `grade`) VALUES
(1, 1, 28),
(1, 2, 27),
(1, 3, 27),
(1, 4, 25),
(2, 1, 26),
(2, 2, 22),
(2, 3, 23),
(2, 4, 29),
(3, 1, 26),
(3, 2, 21),
(3, 3, 18),
(3, 4, 10),
(4, 1, 28),
(4, 2, 25),
(4, 3, 26),
(4, 4, 24);

-- --------------------------------------------------------

--
-- Table structure for table `STUDENTS`
--

CREATE TABLE `STUDENTS` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `surname` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `STUDENTS`
--

INSERT INTO `STUDENTS` (`id`, `name`, `surname`) VALUES
(1, 'Mario', 'Rossi'),
(2, 'Luigi', 'Verdi'),
(3, 'Andrea', 'Grigi'),
(4, 'Stefano', 'Neri');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `COURSES`
--
ALTER TABLE `COURSES`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `GRADES`
--
ALTER TABLE `GRADES`
  ADD PRIMARY KEY (`student_id`,`course_id`);
--
-- Indexes for table `STUDENTS`
--
ALTER TABLE `STUDENTS`
  ADD PRIMARY KEY (`id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `GRADES`
--
ALTER TABLE `GRADES`
  ADD CONSTRAINT `fk_foreign_key_course_id` FOREIGN KEY (`course_id`) REFERENCES `COURSES` (`Id`),
  ADD CONSTRAINT `fk_foreign_key_student_id` FOREIGN KEY (`student_id`) REFERENCES `STUDENTS` (`id`);


-- queries
-- a) SELECT * FROM STUDENTS
-- b)SELECT DISTINCT student_id FROM GRADES WHERE grade > 26
 